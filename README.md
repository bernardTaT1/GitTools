# GitTools

Here are some _Git_ extensions and hooks.

## Git extensions

A _Git_ extension is a program written in any language and following these simple rules:

- the name is in the form `git-EXTENSION` to be called as any _Git_ commands: `git EXTENSION`,
- it must be in the `PATH`,
- a _man page_ (in section 1) allows `git EXTENSION --help` to show something,
- the _man page_ must be in the `MANPATH`.

That's all.
